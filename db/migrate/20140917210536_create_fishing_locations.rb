class CreateFishingLocations < ActiveRecord::Migration
  def change
    create_table :fishing_locations do |t|
      t.string :title
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
