class CreateSpots < ActiveRecord::Migration
  def change
    create_table :spots do |t|
      t.integer :fishingLocationId
      t.string :comment
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
