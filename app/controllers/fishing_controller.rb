class FishingController < ApplicationController
  def index
    @fishing_location = FishingLocations.new
    @locations = FishingLocations.all
    @hash = Gmaps4rails.build_markers(@locations) do |locations, marker|
      if(locations.latitude != nil and locations.longitude != nil)
        marker.lat locations.latitude
        marker.lng locations.longitude
        marker.infowindow render_to_string(:partial => "fishing/link_to_spots", locals: {id: locations.id, title: locations.title})
      end
    end
  end
  def create
    @fishing_location = FishingLocations.new(location_params)
    @fishing_location.save
    redirect_to '/'
  end

  def create_spot
    puts spot_params.inspect
    @fishing_spot = Spots.new(spot_params)
    @fishing_spot.save
    redirect_to '/fishing/'+ params[:id]
  end

  def show
    @fishing_spot = Spots.new
    @locations = Spots.where(fishingLocationId: params[:id])
    @hash = Gmaps4rails.build_markers(@locations) do |locations, marker|
      if(locations.latitude != nil and locations.longitude != nil)
        marker.lat locations.latitude
        marker.lng locations.longitude
        marker.infowindow locations.comment
      end
    end

  end

  def location_params
    params.require(:fishing_locations).permit(:latitude, :longitude, :title)
  end

  def spot_params
    params.require(:spots).permit(:fishingLocationId, :latitude, :longitude, :comment)
  end
end
